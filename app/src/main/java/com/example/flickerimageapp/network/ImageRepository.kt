package com.example.flickerimageapp.network

import com.example.flickerimageapp.data.FlickrResponse
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

class ImageRepository {
    private val apiService: FlickrApiService

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.flickr.com/services/feeds/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(FlickrApiService::class.java)
    }

    suspend fun searchImages(query: String): FlickrResponse {
        return apiService.searchImages(query).apply {
            items.forEachIndexed { index, imageModel ->
                imageModel.id = index.toString() // Assign a unique identifier
            }
        }
    }
}


