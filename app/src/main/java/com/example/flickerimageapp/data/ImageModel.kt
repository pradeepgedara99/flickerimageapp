package com.example.flickerimageapp.data

import android.os.Parcelable
import java.io.Serializable


data class ImageModel(
    var id: String, // Change to var
    val title: String,
    val link: String,
    val media: Media,
    val date_taken: String,
    val description: String,
    val published: String,
    val author: String
) : Serializable

data class Media(val m: String) : Serializable

data class FlickrResponse(
    val title: String,
    val link: String,
    val description: String,
    val modified: String,
    val generator: String,
    val items: List<ImageModel>
)