package com.example.flickerimageapp.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.flickerimageapp.data.ImageModel
import com.example.flickerimageapp.network.ImageRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch


class SearchViewModel(private val repository: ImageRepository = ImageRepository()) : ViewModel() {
    private val _images = MutableStateFlow<List<ImageModel>>(emptyList())
    val images: StateFlow<List<ImageModel>> get() = _images

    private val _isLoading = MutableStateFlow(false)
    val isLoading: StateFlow<Boolean> get() = _isLoading

    fun searchImages(query: String) {
        viewModelScope.launch {
            _isLoading.value = true
            val response = repository.searchImages(query)
            _images.value = response.items
            _isLoading.value = false
        }
    }

    fun getImageById(id: String): Flow<ImageModel?> {
        return _images.map { images -> images.find { it.id == id } }
    }
}


