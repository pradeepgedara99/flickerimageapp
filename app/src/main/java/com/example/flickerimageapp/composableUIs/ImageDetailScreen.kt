package com.example.flickerimageapp.composableUIs

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import coil.compose.rememberImagePainter
import com.example.flickerimageapp.data.ImageModel
import com.example.flickerimageapp.viewmodels.SearchViewModel

@Composable
fun ImageDetailScreen(viewModel: SearchViewModel, imageId: String) {
    val image by viewModel.getImageById(imageId).collectAsState(initial = null)

    image?.let {
        Column(modifier = Modifier.padding(16.dp)) {
            val painter = rememberImagePainter(it.media.m)
            Image(
                painter = painter,
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.fillMaxWidth().height(300.dp)
            )
            Spacer(modifier = Modifier.height(16.dp))
            Text(text = it.title, textAlign = TextAlign.Center)
            Text(text = it.author)
            Text(text = it.published)
            Text(text = it.description)
        }
    } ?: Text("Loading...")
}





