package com.example.flickerimageapp.composableUIs

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import coil.compose.rememberImagePainter
import com.example.flickerimageapp.data.ImageModel
import com.example.flickerimageapp.viewmodels.SearchViewModel

@Composable
fun SearchScreen(navController: NavHostController, viewModel: SearchViewModel) {
    val searchTextState = remember { mutableStateOf(TextFieldValue("")) }
    Column {
        TextField(
            value = searchTextState.value,
            onValueChange = {
                searchTextState.value = it
                viewModel.searchImages(it.text)
            },
            label = { Text("Search") },
            modifier = Modifier.fillMaxWidth().padding(16.dp)
        )
        val images by viewModel.images.collectAsState()
        if (viewModel.isLoading.collectAsState().value) {
            CircularProgressIndicator(modifier = Modifier.padding(16.dp))
        }
        LazyColumn {
            items(images.size) { image ->
                ImageItem(images.get(image)) {
                    navController.navigate("detail/${images.get(image).id}")
                }
            }
        }
    }
}

@Composable
fun ImageItem(image: ImageModel, onClick: () -> Unit) {
    val painter = rememberImagePainter(data = image.media.m)
    Box(
        modifier = Modifier
            .padding(8.dp)
            .clickable(onClick = onClick)
            .fillMaxWidth()
    ) {
        Image(
            painter = painter,
            contentDescription = null,
            contentScale = ContentScale.Crop,
            modifier = Modifier
                .fillMaxWidth()
                .height(200.dp)
        )
    }
}
